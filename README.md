# BIM & Scan® Third-Party Library (Xerces-C)

![BIM & Scan](BimAndScan.png "BIM & Scan® Ltd.")

Conan build script for [Xerces-C](https://xerces.apache.org/index.html), the Apache XML project (C++ libraries).

Supports version 3.2.2 (stable).

Requires the [Bincrafters](https://bintray.com/bincrafters/public-conan) Conan repository for third-party dependencies.
