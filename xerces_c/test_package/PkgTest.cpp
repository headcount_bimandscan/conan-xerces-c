/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'README.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>

#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMDocumentType.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/dom/DOMImplementationLS.hpp>
#include <xercesc/dom/DOMNodeIterator.hpp>
#include <xercesc/dom/DOMNodeList.hpp>
#include <xercesc/dom/DOMText.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLUni.hpp>


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'Xerces-C' package test (compilation, linking, and execution).\n";

    try
    {
        xercesc::XMLPlatformUtils::Initialize();
    }
    catch(const xercesc::XMLException& ex)
    {
        char* msg = xercesc::XMLString::transcode(ex.getMessage());
        std::cerr << "XML toolkit initialisation error: " << msg << std::endl;

        xercesc::XMLString::release(&msg);
        return EXIT_FAILURE;
    }

    std::cout << "'Xerces-C' package works!" << std::endl;
    return EXIT_SUCCESS;
}
