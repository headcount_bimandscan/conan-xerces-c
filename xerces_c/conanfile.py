#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
import os

from conans.model.conan_file import ConanFile

from conans import CMake, \
                   tools


class XercesC(ConanFile):
    name = "xerces_c"
    version = "3.2.2"
    license = "Apache-2.0"
    url = "https://bitbucket.org/headcount_bimandscan/conan-xerces-c"
    description = "The Apache XML project (C++ libraries)."
    generators = "cmake"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    homepage = "https://xerces.apache.org/index.html"

    _src_dir = "xerces_c_src"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    options = {
                  "shared": [
                                True,
                                False
                            ],
                  "fPIC": [
                              True,
                              False
                          ]
              }

    default_options = "shared=False", \
                      "fPIC=True"

    exports = "../LICENCE.md"
    exports_sources = "CMakeLists.txt"

    requires = "icu/62.1@bincrafters/stable"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        zip_name = f"xerces-c-{self.version}"
        zip_name2 = f"{zip_name}.zip"
        download_uri = f"https://archive.apache.org/dist/xerces/c/3/sources/{zip_name2}"

        tools.download(download_uri,
                       zip_name2)
        tools.unzip(zip_name2)

        os.rename(zip_name,
                  self._src_dir)

        os.unlink(zip_name2)
        src_cmakefile = f"{self._src_dir}/CMakeLists.txt"

        # Disable non-library component(s):
        tools.replace_in_file(src_cmakefile,
                              "add_subdirectory(doc)",
                              "#add_subdirectory(doc)")

        tools.replace_in_file(src_cmakefile,
                              "add_subdirectory(samples)",
                              "#add_subdirectory(samples)")

        tools.replace_in_file(src_cmakefile,
                              "add_subdirectory(tests)",
                              "#add_subdirectory(tests)")

    def configure_cmake(self):
        cmake = CMake(self)

        # Configure CMake library build:
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.package_folder
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        cmake.definitions["transcoder"] = "icu" # always use ICU
        cmake.definitions["threads"] = True
        cmake.definitions["network"] = False # disable networking support
        cmake.definitions["message-loader"] = "inmemory"

        if self.settings.os != "Windows":
            cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = self.options.fPIC

        return cmake

    def build(self):
        cmake = self.configure_cmake()
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = self.configure_cmake()
        cmake.install()

        self.copy("LICENSE",
                  "licenses",
                  self._src_dir)

        # Fix for static linking (orphaned symlink):
        if self.settings.os == "Linux" and \
           not self.options.shared:

            os.remove(f"{self.package_folder}/lib/libxerces-c.so")

    def package_info(self):
        self.cpp_info.libdirs = [
                                    "lib"
                                ]

        self.cpp_info.includedirs = [
                                        "include"
                                    ]

        self.cpp_info.libs = tools.collect_libs(self)

        if self.settings.os != "Windows":
            self.cpp_info.libs.append("pthread")
            self.cpp_info.cppflags.append("-pthread")
            self.cpp_info.cflags.append("-pthread")

        if self.settings.os == "Linux" and \
           self.settings.compiler == "gcc":

            self.cpp_info.libs.append("m")
