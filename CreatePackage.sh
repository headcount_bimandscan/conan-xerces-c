#!/bin/sh

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#

set -e
conan create --build="missing" "$@" "xerces_c" "bimandscan/stable"
